package com.example.enrico.wordlookupminiproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    EditText englishWord;
    Button translate, exit;
    RecyclerView recyclerView;
    List<String> swahiliWords;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        englishWord = findViewById(R.id.editEnglishWord);
        translate = findViewById(R.id.btnTranslate);
        exit = findViewById(R.id.btnExit);

        swahiliWords = new ArrayList<>();

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        final RecyclerViewAdapter translationAdapter = new RecyclerViewAdapter(this, swahiliWords);
        recyclerView.setAdapter(translationAdapter);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        translate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = englishWord.getText().toString();

                if (TextUtils.isEmpty(input.trim())) {
                    Toast.makeText(MainActivity.this, "Enter an English word for translation", Toast.LENGTH_LONG).show();
                    return;
                }

                List<String> listofwords = translationSearch(input);
                if (listofwords.isEmpty()) {
                    Toast.makeText(MainActivity.this, "No swahili translations found", Toast.LENGTH_SHORT).show();
                } else {
                    translationAdapter.addList(listofwords);
                }

            }
        });
    }

    private List<String> translationSearch(String word) {
        String line = "";
        List<String> wordList = new ArrayList<>();

        try {
            InputStream inputStream = getAssets().open("db.csv");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            while ((line = bufferedReader.readLine()) != null) {
                List<String> words = Arrays.asList(line.split(","));

                if (words.size() == 2) {
                    if (word.toLowerCase().equals(words.get(1).toLowerCase())) {
                        wordList.add(words.get(0));
                    }
                }

                if (wordList.size() == 10) {
                    break;
                }
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return wordList;
    }
}
